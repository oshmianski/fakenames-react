module.exports = function (api) {
    api.cache(true);

    const presets = [
        "@babel/env",
        "@babel/react"
    ];

    const plugins = [
        ["@babel/plugin-proposal-decorators", {"legacy": true}],
        ["@babel/plugin-proposal-class-properties", {"loose": true}],
        "@babel/plugin-proposal-export-default-from",
    ];

    return {
        presets,
        plugins
    };
};