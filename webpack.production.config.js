const HtmlWebpackPlugin = require('html-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');

module.exports = {
    mode: "production",
    optimization: {
        minimize: true,
        minimizer: [new TerserPlugin({
            terserOptions: {
                output: {
                    comments: false,
                }
            },
        })],
        splitChunks: {
            chunks: 'all',
            maxInitialRequests: Infinity,
            minSize: 0,
            automaticNameDelimiter: '_',
            cacheGroups: {
                vendors: {
                    test: /[\\/]node_modules[\\/]/,
                    minChunks: 1,
                    name: "external"
                }
            }
        }
    },
    entry: {
        'bundle.min': './src/main.js',
    },
    output: {
        path: __dirname + '/build/',
        publicPath: '',
        filename: 'build/[name].[hash].js',
        chunkFilename: 'build/[name].[chunkhash].js'
    },
    resolve: {
        extensions: ['.json', '.js', '.jsx'],
        modules: [
            'node_modules',
            'src'
        ]
    },
    module: {
        rules: [
            {
                test: /.jsx?$/, // Match both .js and .jsx
                use: [{
                    loader: 'babel-loader'
                }],
                exclude: /node_modules/,
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: __dirname + '/resources/html_template/index-production.html',
            cache: false,
            chunks: ['bundle.min'],
            xhtml: true,
        })
    ]
};
