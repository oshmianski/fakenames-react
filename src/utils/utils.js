import CONFIG from '../../config.json';

class Utils {
    static ID() {
        // https://gist.github.com/gordonbrander/2230317
        // Math.random should be unique because of its seeding algorithm.
        // Convert it to base 36 (numbers + letters), and grab the first 9 characters
        // after the decimal.
        return '_' + Math.random().toString(36).substr(2, 9);
    }

    static getRouteToApi = () => {
        let route;

        if (window.location.pathname.indexOf('index.html') !== -1) {
            route = window.location.pathname.replace("index.html", '') + '/xsp/services/rest/fakenames/1.0';
        } else {
            route = window.location.origin + '/xsp/services/rest/fakenames/1.0';
        }

        return route;
    };

    static getRouteToApi_RVE = () => {
        let route;

        if (window.location.pathname.indexOf('index.html') !== -1) {
            route = window.location.pathname.replace("index.html", '') + '/';
        } else {
            route = window.location.origin + '/proxy_readviewentries';
        }

        return route;
    };

    static getRoute = (route) => {
        const baseRoute = Utils.getRouteToApi()+"/";

        switch (route) {
            case "name-simple":
                return `${baseRoute}viewData`;

            default:
                return baseRoute;
        }
    };

    static getAuthBasicPass = () => {
        return `Basic ${btoa(`${CONFIG.user}:${CONFIG.pass}`)}`;
    };

    static getAuthHeader = () => {
        let authHeader;

        if (window.location.pathname.indexOf('index.html') !== -1) {
            authHeader = {headers: ""};
        } else {
            authHeader = {headers: {'Authorization': Utils.getAuthBasicPass()}};
        }

        return authHeader;
    };

    static getErrorMessage(payload) {
        let error = '';

        try {

            if (payload === undefined) {
                return error;
            }

            if (payload.response) {

                if (payload.response.status === 404) {
                    error = "Ошибка доступа к серверу!";
                } else if (payload.response.message) {
                    error = payload.response.message;
                } else if (payload.response.data) {
                    if (payload.response.data.result) {
                        error = payload.response.data.result.msg;
                    } else {
                        error = payload.response.data.toString();
                    }
                } else {
                    error = payload.response.data;
                }
            } else if (payload.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                error = payload.request;
            } else if (payload.result) {
                error = payload.result.msg;
            } else {
                if (payload.message) {
                    error = payload.message;
                } else {
                    error = JSON.stringify(payload, Object.getOwnPropertyNames(payload));
                }
            }
        } catch (err) {
            if (err.message) {
                error = err.message;
            } else {
                error = JSON.stringify(err, Object.getOwnPropertyNames(err));
            }
        }

        return error;
    }

    static action(obj, type, payload) {
        return {
            type: `${obj}/${type}`,
            payload: Utils.isObject(payload) ? {...payload} : payload,
        }
    };

    static actionWithPromise = (obj, type, payload) => dispatch => Promise.resolve().then(() => {
        return dispatch({
            type: obj ? `${obj}/${type}` : type,
            payload: Utils.isObject(payload) ? {...payload} : payload,
        });
    });

    static dispatchWithPromise = (func) => dispatch => Promise.resolve().then(() => {
        return dispatch(func);
    });

    static isObject(a) {
        return (!!a) && (a.constructor === Object);
    }
}

export default Utils;