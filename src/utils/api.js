import axios from "axios";

import Utils from "utils/utils";

function loadList(view, start, limit, sortColumn, descending, obj, dispatch) {
    const desc = descending || false;
    const sort = (sortColumn === -1 || sortColumn === undefined || sortColumn === null) ? '' : ("&" + (desc ? `ResortDescending=` : "ResortAscending=") + sortColumn);

    dispatch && dispatch({type: `${obj}/REQUEST`});

    return axios
        .get(`${Utils.getRouteToApi_RVE()}${view}?ReadViewEntries&Outputformat=JSON&start=${start + 1}&count=${limit}${sort}&nocache=${(new Date()).getTime()}`,
            Utils.getAuthHeader())
        .then(response => {
            const {headers} = response;

            if (headers['content-type'].indexOf("text/html") !== -1) {
                throw new Error("WRONG FORMAT RESPONSE");
            }

            dispatch && dispatch({type: `${obj}/SUCCESS`, payload: response});

            return response;
        })
        .catch(error => {
            const errorRet = Utils.getErrorMessage(error);

            dispatch && dispatch({type: `${obj}/FAILURE`, payload: errorRet});

            throw new Error(Utils.getErrorMessage(errorRet))
        })
}

const api = {
    loadList_RVE: (view, start, limit, sortColumn, descending) => {
        return loadList(view, start, limit, sortColumn, descending)
    },

    loadList_RVE_store: (obj) => {
        return (dispatch, getState) => {
            const state = getState()[obj];
            const {view, start, limit, sortColumn, descending} = state;

            return loadList(view, start, limit, sortColumn, descending, obj, dispatch)
        }
    },

    loadNote_store: (obj, params) => {
        return (dispatch) => {
            const action = params[`command`];

            dispatch({type: `${obj}/${action}_REQUEST`});

            return axios.post(`${Utils.getRouteToApi()}/noteData`, {...params, obj}, Utils.getAuthHeader()).then(response => {
                const {headers} = response;

                if (headers['content-type'].indexOf("text/html") !== -1) {
                    throw new Error("WRONG FORMAT RESPONSE");
                }

                dispatch({type: `${obj}/${action}_SUCCESS`, payload: response.data});

                return response;
            }).catch(error => {
                const errorRet = Utils.getErrorMessage(error);

                dispatch({type: `${obj}/${action}_FAILURE`, payload: errorRet});

                throw new Error(Utils.getErrorMessage(error))
            });
        }
    },
};

export default api;