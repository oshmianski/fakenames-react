import React from 'react';

import {makeStyles} from '@material-ui/core/styles';

import {NavLink} from 'react-router-dom';

const useStyles = makeStyles(() => ({
    '@global': {
        html: {
            margin: 0,
            padding: 0,
            width: '100%',
            height: '100%',
            backgroundColor: '#ffffff',
        }
        ,
        '*, *:before, *:after': {
            boxSizing: 'inherit',
        },
        body: {
            margin: 0,
        },
        '#mount-point': {
            position: 'absolute',
            top: 0,
            bottom: 0,
            left: 0,
            right: 0,
        },
        a: {
            color: '#438945',
            textDecoration: 'none',
            '&:hover': {
                textDecoration: 'underline',
            },
        },
    },
    body: {
        position: 'relative',
        display: 'flex',
        width: '100%',
        height: '100%',
        flexDirection: "row",
    },
    nav: {
        width: 200,
        display: 'flex',
        flexDirection: "column",
        padding: 10,
        borderRight: `1px solid #dfdfdf`,
    },
    content: {
        flexGrow: 1,
    },
    navItem: {
        margin: 10,
    },
    navItemActive: {
        fontWeight: "bold",
        color: "#000",
    },
}));

function App(props) {
    const classes = useStyles();

    return (
        <div className={classes.body}>
            <div className={classes.nav}>
                <div className={classes.navItem}>
                    <NavLink to={`/home`} activeClassName={classes.navItemActive}>Home</NavLink>
                </div>
                <div className={classes.navItem}>
                    <NavLink to={`/names-simple`} activeClassName={classes.navItemActive}>Contacts RVE</NavLink>
                </div>
                <div className={classes.navItem}>
                    <NavLink to={`/names-store`} activeClassName={classes.navItemActive}>Contacts RVE store</NavLink>
                </div>
            </div>

            <div className={classes.content}>{props.children}</div>
        </div>
    )
}

export default App;