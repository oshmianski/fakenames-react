import React, {useState, useEffect} from "react";

import {makeStyles} from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

import {Link} from "react-router-dom";

import {useSelector, useDispatch} from 'react-redux';

import api from 'utils/api';
import Utils from 'utils/utils';
import ContactRead from 'components/ContactRead';
import ContactEdit from 'components/ContactEdit';
import {produceDispatcherActions} from 'redux/actions/contact';

const useStyles = makeStyles(() => ({
    body: {
        display: "flex",
        flexDirection: "column",
        height: `calc(100%)`,
    },
    toolbar: {
        height: 55,
        borderBottom: `1px solid #dfdfdf`,
        display: "flex",
        alignItems: `center`,
        justifyContent: "space-between",
        paddingLeft: 10,
    },
    content: {
        height: `calc(100% - 55px)`,
        overflow: "auto",
    },
    loading: {
        margin: 10,
    },
}));

// The usage of React.forwardRef will no longer be required for react-router-dom v6.
// see https://github.com/ReactTraining/react-router/issues/6056
const AdapterLink = React.forwardRef((props, ref) => <Link innerRef={ref} {...props} />);


function Contact(props) {
    const classes = useStyles();

    const contactReducer = useSelector(state => state[`contact`]);
    const {note, fetching} = contactReducer;
    const dispatch = useDispatch();
    const [ediMode, setEditMode] = useState(false);
    const contactActions = produceDispatcherActions(dispatch);

    useEffect(() => {
        dispatch(api.loadNote_store(`contact`, {command: `READ_NOTE`, unid: props.id,}))
    }, []);

    function updateCategoryList() {
        //set flag to update category views
        dispatch({type: "contacts/SET_CHANGED", payload: true});

    }

    function saveNote() {
        Utils.dispatchWithPromise(dispatch)(api.loadNote_store(`contact`, {command: `UPDATE_NOTE`, unid: props.id, note}))
            .then(() => {
                updateCategoryList();
            })
            .catch(error => {
                console.info(Utils.getErrorMessage(error));
            })
    }

    function editNote() {
        Utils.dispatchWithPromise(dispatch)(api.loadNote_store(`contact`, {command: `EDIT_NOTE`, unid: props.id}))
            .then((response) => {
                setEditMode(response.data.data[`editModeServer`]);
            })
            .catch(error => {
                console.info(Utils.getErrorMessage(error));
            })
    }

    function setNote(note) {
        contactActions.setNote(note);
    }

    return (
        <div className={classes.body}>
            <div className={classes.toolbar}>
                <div style={{display: 'flex'}}>
                    <div style={{marginRight: 10}}>
                        <Button to={`/${props.view}`} component={AdapterLink} disabled={fetching} variant={"contained"} size={"small"}>Exit</Button>
                    </div>
                    {ediMode && <div style={{marginRight: 10}}>
                        <Button onClick={saveNote} disabled={fetching} variant={"contained"} size={"small"}>Save</Button>
                    </div>}
                    {!ediMode && <div style={{marginRight: 10}}>
                        <Button onClick={editNote} disabled={fetching} variant={"contained"} size={"small"}>Edit</Button>
                    </div>}
                </div>
                <div>
                    {fetching && <div className={classes.loading}>Loading...</div>}
                </div>
            </div>
            <div className={classes.content}>
                {ediMode ? <ContactEdit note={note} fetching={fetching} setNote={setNote}/> : <ContactRead note={note} fetching={fetching}/>}
            </div>
        </div>
    )
}

export default Contact;