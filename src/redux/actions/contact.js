import Utils from "utils/utils";

const obj = "contact";

const setNote = (note) => Utils.actionWithPromise(obj, `SET_NOTE`, note);

export const produceDispatcherActions = (dispatch) => {
    return {
        setNote: (note) => {
            return dispatch(setNote(note))
        },
    }
};