import Utils from "utils/utils";

const obj = "contacts";

const setPage = (page) => Utils.actionWithPromise(obj, "SET_PAGE", page);
const setScrollTo = (scrollTo) => Utils.actionWithPromise(obj, "SET_SCROLL_TO", scrollTo);

export const produceDispatcherActions = (dispatch) => {
    return {
        setPage: (page) => {
            return dispatch(setPage(page))
        },
        setScrollTo: (scrollTo) => {
            return dispatch(setScrollTo(scrollTo))
        },
  }
};