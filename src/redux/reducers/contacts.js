const obj = "contacts";

const initialState = {
    obj: `contacts`,
    records: [],
    count: 0,
    page: 1,
    start: 0,
    pages: 0,
    limit: 50,
    view: `contacts`,
    sortColumn: 1,
    descending: false,
    fetching: false,
    error: '',
    changed: true,
    scrollTo: 0,
};

export default function (state = initialState, action) {
    switch (action.type) {
        case `${obj}/REQUEST`:
            return {
                ...state,
                fetching: true,
                error: '',
            };

        case `${obj}/SUCCESS`:
            const {data} = action.payload;

            const records = data[`viewentry`].map(entry => {
                return Object.assign({}, {
                    [`@unid`]: entry[`@unid`],
                    [`@position`]: entry[`@position`],
                }, ...entry[`entrydata`].map(ed => {
                    const value = ed[`text`] || ed[`datetime`];

                    return {
                        [ed[`@name`]]: Object.keys(value).map(val => value[val]).join(`\n`)
                    }
                }))
            });

            const count = data[`@toplevelentries`];

            const pages = ((count / state.limit >> 0) + ((count % state.limit) > 0 ? 1 : 0));

            return {
                ...state,
                error: '',
                records,
                count,
                pages,
                fetching: false,
                changed: false,
            };

        case `${obj}/FAILURE`:
            return {
                ...state,
                error: action.payload,
                fetching: false,
            };

        case `${obj}/SET_PAGE`:
            const page = action.payload;
            return {
                ...state,
                page,
                start: (page - 1) * state.limit,
            };

        case `${obj}/SET_CHANGED`:
            return {
                ...state,
                changed: action.payload,
            };

        case `${obj}/SET_SCROLL_TO`:
            return {
                ...state,
                scrollTo: action.payload,
            };

        default:
            return state;
    }
}
