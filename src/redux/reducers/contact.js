const obj = `contact`;

let initialState = {
    draw: 0,
    error: '',
    fetching: false,
    note: {},
    other: {},
};

function reducer(state = initialState, action) {
    switch (action.type) {
        case `${obj}/CREATE_NOTE_REQUEST`:
        case `${obj}/READ_NOTE_REQUEST`:
        case `${obj}/UPDATE_NOTE_REQUEST`:
        case `${obj}/DELETE_NOTE_REQUEST`:
        case `${obj}/EDIT_NOTE_REQUEST`:
            return {
                ...state,
                fetching: true,
                error: '',
            };

        case `${obj}/CREATE_NOTE_SUCCESS`:
        case `${obj}/READ_NOTE_SUCCESS`:
        case `${obj}/UPDATE_NOTE_SUCCESS`:
            const {payload: {data, ...other}} = action;
            const {note} = data;

            return {
                ...state,
                error: '',
                fetching: false,
                note,
                other,
            };

        case `${obj}/EDIT_NOTE_SUCCESS`:
        case `${obj}/CHECK_DB_ACCESS_SUCCESS`:
        case `${obj}/DELETE_NOTE_SUCCESS`:
            return {
                ...state,
                error: '',
                fetching: false,
            };


        case `${obj}/CREATE_NOTE_FAILURE`:
        case `${obj}/READ_NOTE_FAILURE`:
        case `${obj}/UPDATE_NOTE_FAILURE`:
        case `${obj}/DELETE_NOTE_FAILURE`:
        case `${obj}/EDIT_NOTE_FAILURE`:
        case `${obj}/CHECK_DB_ACCESS_FAILURE`:
            const {payload: {result}} = action;

            return {
                ...state,
                error: (result === undefined || result === null) ? `Error note loading` : result.msg,
                fetching: false,
            };

        case `${obj}/SET_NOTE`:
            return {
                ...state,
                note: action.payload
            };

        default:
            return state
    }
}

export default reducer;