import {combineReducers} from "redux";

import contacts from "./contacts";
import contact from "./contact";

export default combineReducers({
    contacts,
    contact,
});