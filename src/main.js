import "babel-polyfill";    //for IE 11

import React from 'react';
import ReactDOM from 'react-dom';

import {HashRouter as Router} from 'react-router-dom';
import {Switch, Route, Redirect} from 'react-router';

import {Provider} from 'react-redux';

import store from 'redux/store';

import App from 'containers/App';
import ContactsSimple from 'components/ContactsSimple';
import ContactsStore from 'components/ContactsStore';
import Home from 'components/Home';
import Contact from 'containers/Contact';

function Main() {
    return <Provider store={store}>
        <Router>
            <App>
                <Switch>
                    <Route exact path='/home' component={Home}/>
                    <Route exact path='/names-simple' component={ContactsSimple}/>
                    <Route exact path='/names-store' render={(props) => <ContactsStore key={`names-store-edit`} canEdit routeProps={props}/>}/>
                    <Route exact path='/:view/:id' render={({match}) => <Contact view={match.params.view} id={match.params.id}/>}/>
                    <Redirect from='/' to='/home'/>
                </Switch>
            </App>
        </Router>
    </Provider>
}

ReactDOM.render(<Main/>, document.getElementById('mount-point'));
