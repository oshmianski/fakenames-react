import React from "react";

import {makeStyles} from '@material-ui/core/styles';
import {TextField} from "@material-ui/core";

const useStyles = makeStyles(() => ({
    body: {
        margin: 10,
        display: 'flex',
        flexDirection: 'column',
    },
}));

function ContactEdit(props) {
    const classes = useStyles();

    const {note, fetching, setNote} = props;

    function handlerChangeInput(name, event) {
        setNote({...note, [name]: event.target.value})
    }

    return (
        <div className={classes.body}>
            <div style={{width: 200}}>
                <TextField
                    autoFocus
                    fullWidth
                    label={`firstName`}
                    defaultValue={note[`firstName`] || ''}
                    onChange={(event) => handlerChangeInput('firstName', event)}
                    name={`firstName`}
                    disabled={fetching}
                />
            </div>
            <div style={{width: 200, marginTop: 20}}>
                <TextField
                    fullWidth
                    label={`lastName`}
                    defaultValue={note[`lastName`] || ''}
                    onChange={(event) => handlerChangeInput('lastName', event)}
                    name={`lastName`}
                    disabled={fetching}
                />
            </div>
        </div>
    )
}

export default ContactEdit;