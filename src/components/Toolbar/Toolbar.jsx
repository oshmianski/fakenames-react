import React from "react";

import {makeStyles} from '@material-ui/core/styles';
import Button from "@material-ui/core/Button";

const useStyles = makeStyles(() => ({
    toolbar: {
        padding: 5,
        height: 55,
        borderBottom: `1px solid #dfdfdf`,
        display: "flex",
        alignItems: `center`,
        justifyContent: "space-between",
    },
    loading: {
        margin: 10,
    },
}));

function Toolbar({start, limit, page, pages, fetching, count, setPage}) {
    const classes = useStyles();

    return (
        <div className={classes.toolbar}>
            <div>{`records from ${start + 1} to ${start + limit} of ${count}, page ${page} of ${pages}`}</div>

            {fetching && <div className={classes.loading}>Loading...</div>}

            <div style={{display: "flex"}}>
                {page > 1 && <Button disabled={fetching} className={classes.divButton} onClick={() => setPage(page - 1)}>Prev</Button>}
                {page < pages && <Button disabled={fetching} className={classes.divButton} onClick={() => setPage(page + 1)}>Next</Button>}
            </div>
        </div>
    )
}

export default Toolbar;