import React from "react";

import {makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
    body: {
        margin: 10,
    },
}));

function Home() {
    const classes = useStyles();

    return (
        <div className={classes.body}>Home</div>
    )
}

export default Home;