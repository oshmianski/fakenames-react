import React from "react";

import {makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
    body: {
        margin: 10,
        display: 'flex',
        flexDirection: 'column',
    },
}));

function ContactRead(props) {
    const classes = useStyles();

    const {note} = props;

    return (
        <div className={classes.body}>
            <div style={{margin: 10, display: 'flex', flexDirection: "row"}}>
                <div style={{marginRight: 10}}>firstName:</div>
                <div>{note ? note[`firstName`] : null}</div>
            </div>
            <div style={{margin: 10, display: 'flex', flexDirection: "row"}}>
                <div style={{marginRight: 10}}>lastName:</div>
                <div>{note ? note[`lastName`] : null}</div>
            </div>
        </div>
    )
}

export default ContactRead;