import React, {useState, useEffect} from "react";

import {makeStyles} from '@material-ui/core/styles';

import api from 'utils/api';
import Toolbar from "../Toolbar";

const useStyles = makeStyles(() => ({
    body: {
        display: "flex",
        flexDirection: "column",
        height: `calc(100%)`,
    },
    toolbar: {
        height: 55,
        borderBottom: `1px solid #dfdfdf`,
        display: "flex",
        alignItems: `center`,
        justifyContent: "space-between",
    },
    list: {
        height: `calc(100% - 55px)`,
        overflow: "auto",
    },
    person: {
        margin: 0,
        display: "flex",
        flexDirection: "row",
        '&>div': {
            margin: 5.
        },
        '&:nth-child(2n)': {
            backgroundColor: 'rgb(0, 0, 0, 0.04)'
        }
    },
    loading: {
        margin: 10,
    },
    divButton: {
        marginRight: 10,
        cursor: "pointer",
        userSelect: "none",
    },
}));

function ContactsSimple() {
    const classes = useStyles();

    const [data, setData] = useState({});
    const [fetching, setFetching] = useState(false);
    const [page, setPage] = useState(1);
    const limit = 50;
    const start = (page - 1) * limit;
    const view = `contacts`;
    const sortColumn = 1;
    const descending = false;
    const records = data[`records`] || [];
    const count = data[`count`] || 0;
    const pages = ((count / limit >> 0) + ((count % limit) > 0 ? 1 : 0));

    useEffect(() => {
        setFetching(true);

        api.loadList_RVE(view, start, limit, sortColumn, descending)
            .then(response => {
                const data = response['data'];

                const records = data[`viewentry`].map(entry => {
                    return Object.assign({}, {
                        [`@unid`]: entry[`@unid`],
                        [`@position`]: entry[`@position`],
                    }, ...entry[`entrydata`].map(ed => {
                        const value = ed[`text`] || ed[`datetime`];

                        return {
                            [ed[`@name`]]: Object.keys(value).map(val => value[val]).join(`\n`)
                        }
                    }))
                });

                setData({count: data[`@toplevelentries`], records})
            })
            .catch(error => {
                console.warn(`name-simple post`, error);
            })
            .finally(() => {
                setFetching(false);
            });
    }, [page]);

    return (
        <div className={classes.body}>
            <Toolbar limit={limit} page={page} pages={pages} start={start} count={count} fetching={fetching} setPage={setPage}/>

            <div className={classes.list}>
                {records.map(record =>
                    <div key={record[`@unid`]} className={classes.person}>
                        <div>
                            {record[`lastName`]}
                        </div>
                        <div>
                            {record[`firstName`]}
                        </div>
                    </div>
                )}
            </div>
        </div>
    )
}

export default ContactsSimple;