import React, {useLayoutEffect, useEffect, useRef} from "react";

import {makeStyles} from '@material-ui/core/styles';

import {useSelector, useDispatch} from 'react-redux';

import {Link} from 'react-router-dom';

import {Scrollbars} from 'react-custom-scrollbars';

import api from 'utils/api';
import {produceDispatcherActions} from 'redux/actions/contacts';
import Toolbar from "../Toolbar";

const useStyles = makeStyles(() => ({
    body: {
        display: "flex",
        flexDirection: "column",
        height: `calc(100%)`,
    },
    list: {
        height: `calc(100% - 55px)`,
        overflow: "auto",
    },
    person: {
        margin: 0,
        display: "flex",
        flexDirection: "row",
        '&>div': {
            margin: 5.
        },
        '&:nth-child(2n)': {
            backgroundColor: 'rgb(0, 0, 0, 0.04)'
        }
    },
}));

function ContactsStore(props) {
    const refList = useRef(null);
    const classes = useStyles();

    const {canEdit} = props;
    const from = props[`routeProps`] ? props[`routeProps`].location.pathname : '';
    const contactsReducer = useSelector(state => state[`contacts`]);
    const {fetching, page, pages, start, limit, records, count, changed, obj, scrollTo} = contactsReducer;

    const dispatch = useDispatch();

    const contactsActions = produceDispatcherActions(dispatch);

    useLayoutEffect(() => {
        if (refList && refList.current) {
            refList['current'].scrollTop(scrollTo);
        }

        return () => {
            contactsActions.setScrollTo(refList['current'].getScrollTop());
        }
    }, []);

    useEffect(() => {
        changed && dispatch(api.loadList_RVE_store(obj));
    }, []);


    function setPage(page) {
        contactsActions.setPage(page).then(() => {
            dispatch(api.loadList_RVE_store(obj));
            refList['current'].scrollToTop();
        });
    }

    return (
        <div className={classes.body}>
            <Toolbar limit={limit} page={page} pages={pages} start={start} count={count} fetching={fetching} setPage={setPage}/>

            <Scrollbars style={{width: "100%", height: `calc(100% - 55px)`, display: 'flex', flexDirection: 'column'}} ref={refList}>
                {records.map(record =>
                    <div key={record[`@unid`]} className={classes.person}>
                        <div>
                            {!canEdit ? record[`lastName`] : <Link to={`${from}/${record[`@unid`]}`}>{record[`lastName`]}</Link>}
                        </div>
                        <div>
                            {record[`firstName`]}
                        </div>
                    </div>
                )}
            </Scrollbars>
        </div>
    )
}

export default ContactsStore;