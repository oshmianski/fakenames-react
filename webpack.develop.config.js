const HtmlWebpackPlugin = require('html-webpack-plugin');

const CONFIG = require('./config.json');

module.exports = {
    mode: "development",
    entry: {
        'bundle': './src/main.js',
    },
    output: {
        path: __dirname + '/public/build/',
        publicPath: '',
        filename: 'build/[name].[hash].js',
        chunkFilename: 'build/[name].[chunkhash].js'
    },
    resolve: {
        extensions: ['.json', '.js', '.jsx'],
        modules: [
            'node_modules',
            'src'
        ]
    },
    devServer: {
        historyApiFallback: true,
        disableHostCheck: true,
        proxy: {
            '/xsp/services': {
                target: {
                    host: CONFIG.db.host,
                    protocol: CONFIG.db.protocol,
                    port: CONFIG.db.port,
                },
                pathRewrite: {"^/xsp/services": `${CONFIG.db.path}/xsp/services`}
            },
            '/proxy_readviewentries': {
                target: {
                    host: CONFIG.db.host,
                    protocol: CONFIG.db.protocol,
                    port: CONFIG.db.port,
                },
                pathRewrite: {"^/proxy_readviewentries": `${CONFIG.db.path}/`}
            },
        }
    },
    devtool: "source-map",
    module: {
        rules: [
            {
                test: /.jsx?$/, // Match both .js and .jsx
                use: [{
                    loader: 'babel-loader'
                }],
                exclude: /node_modules/,
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: __dirname + '/resources/html_template/index-dev.html',
            hash: true,
            cache: false,
        })
    ]
};
